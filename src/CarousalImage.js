import React, {Component} from 'react';
import logo from './logo.svg';
import './App.css';

class App extends Component {
    constructor(props) {
        super();
        this.state = {}
    }
    onThumbClick(event) {
        console.log('Thump clicked');
        console.log(event);
    }
    render() {
        let one = {
            'foo': 'bar'
        }
        let two = {
            'xxx': 'uuu'
        }
        return (
            <div className="App">
                <div className="App-header">
                    <img src={logo} className="App-logo" alt="logo"/>
                    <h2>Welcome to React</h2>
                </div>
                <p className="App-intro">
                    To get started, edit
                    <code>src/App.js</code>
                    and save to reload.
                </p>
                <div className="container">
                    <div className="row">
                        <div className="col-sm-8 col-sm-offset-2 bgcolor-grey">.col-md-6 .col-md-offset-3</div>
                    </div>
                    <div className="row pd-t-20">
                        <div className="col-sm-2">
                            <h3>Column 1</h3>
                            <p>
                                <a onClick={this.onThumbClick.bind(this, one)}>Lorem ipsum dolor..</a>
                            </p>
                        </div>
                        <div className="col-sm-2">
                            <h3>Column 2</h3>
                            <p>
                                <a onClick={(event) => this.onThumbClick(two)}>Ut enim ad..</a>
                            </p>
                        </div>
                        <div className="col-sm-2">
                            <h3>Column 3</h3>
                            <p>Ut enim ad..</p>
                        </div>
                        <div className="col-sm-2">
                            <h3>Column 4</h3>
                            <p>Ut enim ad..</p>
                        </div>
                        <div className="col-sm-2">
                            <h3>Column 5</h3>
                            <p>Ut enim ad..</p>
                        </div>
                        <div className="col-sm-2">
                            <h3>Column 6</h3>
                            <p>Ut enim ad..</p>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default App;
