import React, {Component} from 'react';
import './App.css';

class Carousel extends Component {
    constructor(props) {
        super();
        this.state = {}
    }
    onThumbClick(event) {
        this.props.changeHeroImage(event)
    }
    render() {
        return (
            <div className="row pd-t-20">
                {this.props.imgData.map((unit,i)  => {
                    return <div key={i} className="col-sm-2 col-xs-4">
                        <p>
                            <a onClick={this.onThumbClick.bind(this, unit)}>
                                <img src={unit.thumb} /><br/>Ut enim ad..
                            </a>
                        </p>
                    </div>
                })}
            </div>
        );
    }
}

export default Carousel;
