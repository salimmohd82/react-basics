import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import jsonData from './file/data.json';

ReactDOM.render(<App imgData={jsonData.images} defaultPos={1} />, document.getElementById('root'));
registerServiceWorker();
