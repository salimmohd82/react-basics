import React from 'react';

// const Heroimage = (props) => {
//     return (
//         <div className="row">
//             <div className="col-sm-2 fs-50 pd-t-25p"><i className="fa fa-angle-left" aria-hidden="true"></i></div>
//             <div className="col-sm-8 bgcolor-grey"><img src={props.imageNow}/></div>
//             <div className="col-sm-2 fs-50 pd-t-25p"><i className="fa fa-angle-right" aria-hidden="true"></i></div>
//         </div>
//     );
// }

class Heroimage extends React.Component {
    constructor(props) {
        super();
    }

    scrollImage(event) {
        console.log('move ' + event);
    }
    
    render() {
        return (
            <div className="row">
                <div className="col-sm-2 fs-50 fa-5 pd-t-25p"><i onClick={(event) => this.scrollImage("left")} className="fa fa-angle-left" aria-hidden="true"></i></div>
                <div className="col-sm-8 bgcolor-grey"><img src={this.props.imageNow}/></div>
                <div className="col-sm-2 fs-50 pd-t-25p"><i onClick={(event) => this.scrollImage("right")} className="fa fa-angle-right" aria-hidden="true"></i></div>
            </div>
        );
    }
}

export default Heroimage;
