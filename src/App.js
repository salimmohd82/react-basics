import React, {Component} from 'react';
import logo from './logo.svg';
import './App.css';
import Heroimage from './HeroImage';
import Carousel from './Carousal';

class App extends Component {
    constructor(props) {
        super();
        this.state = {
            currentImage: props.imgData[props.defaultPos].img
        };
        console.log(props.imgData + 'b');
    }

    onThumbClick(event) {
        this.setState({currentImage: event.img});
    }

    componentWillReceiveProps(nextProps) {
        console.log("Component will receive props", nextProps);
    }

    shouldComponentUpdate(nextProps, nextState) {
        console.log("Should Component update", nextProps, nextState);
        if (nextState.status === 1) {
            return false;
        }
        // return true;
    }

    componentWillUpdate(nextProps, nextState) {
        console.log("Component will update", nextProps, nextState);
    }

    componentDidUpdate(prevProps, prevState) {
        console.log("Component did update", prevProps, prevState);
    }

    render() {
        return (
            <div className="App">
                <div className="App-header">
                    <img src={logo} className="App-logo" alt="logo"/>
                    <h2>Welcome to React</h2>
                </div>
                <p className="App-intro">
                    To get started, edit
                    <code>src/App.js</code>
                    and save to reload.
                </p>
                <div className="container">
                    <Heroimage imageNow={this.state.currentImage}/>
                    <Carousel imgData={this.props.imgData} changeHeroImage={this.onThumbClick.bind(this)}/>
                </div>
            </div>
        );
    }
}

export default App;
